import axios from "axios";

export const DECODE_SUCCESS = 'DECODE_SUCCESS';
export const decodeSuccess = payload => ({type: DECODE_SUCCESS, payload});
export const decodeRequest = payload => {
    return async dispatch => {
        const response = await axios.post('http://localhost:8000/decode', payload);
        dispatch(decodeSuccess(response.data.decoded));
    }
};

export const ENCODE_SUCCESS = 'ENCODE_SUCCESS';
export const encodeSuccess = payload => ({type: ENCODE_SUCCESS, payload});
export const encodeRequest = (payload) => {
    console.log('encode!!')
    return async dispatch => {
        const response = await axios.post('http://localhost:8000/encode', payload);
        dispatch(encodeSuccess(response.data.encoded));
    }
};