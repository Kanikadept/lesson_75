import {DECODE_SUCCESS, ENCODE_SUCCESS} from "../actions/actions";

const initialState = {
    messageDecoded: null,
    messageEncoded: null
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case DECODE_SUCCESS:
            return {...state, messageDecoded: action.payload};
        case ENCODE_SUCCESS:
            return {...state, messageEncoded: action.payload};
        default:
            return state;
    }
}

export default reducer;