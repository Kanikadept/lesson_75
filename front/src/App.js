import './App.css';
import EncodeDecode from "./containers/EncodeDecode";

const App = () => (
    <div className="App">
        <EncodeDecode/>
    </div>
);

export default App;
