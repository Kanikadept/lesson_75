import React, {useState, useEffect} from 'react';
import './EncodeDecode.css';
import {useDispatch, useSelector} from "react-redux";
import {decodeRequest, encodeRequest} from "../store/actions/actions";

const EncodeDecode = () => {

    const dispatch = useDispatch();
    const messageEncoded = useSelector(state => state.messageEncoded);
    const messageDecoded = useSelector(state => state.messageDecoded);

    const [payload, setPayload] = useState({
        password : '',
        messageEncoded: '',
        messageDecoded: ''
    });

    useEffect(() => {
        if(messageEncoded) {
            const payloadCopy = {...payload};
            payloadCopy.messageEncoded = messageEncoded;
            setPayload(payloadCopy);
        }
    }, [messageEncoded]);

    useEffect(() => {
        if(messageDecoded) {
            const payloadCopy = {...payload};
            payloadCopy.messageDecoded = messageDecoded;
            setPayload(payloadCopy);
        }
    }, [messageDecoded])

    const handleChange = (e) => {
        const {name, value} = e.target;
        const payloadCopy = {...payload};
        payloadCopy[name] = value;
        setPayload(payloadCopy);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        const requestPayload = {password: payload.password, message: payload.messageDecoded}
        dispatch(encodeRequest(requestPayload));
    }

    const handleDecode = (e) => {

        const requestPayload = {password: payload.password, message: payload.messageEncoded}
        dispatch(decodeRequest(requestPayload));
    }

    return (
        <form className="encode-decode" onSubmit={handleSubmit}>
            <div className="form-row">
                <label>Decode message</label>
                <textarea value={payload.messageDecoded} onChange={handleChange} name="messageDecoded" id="messageDecoded" cols="30" rows="10"/>
            </div>
            <div className="form-row">
                <label>Password</label>
                <div className="form-btns">
                    <input value={payload.password} name="password" onChange={handleChange} type="text"  required/>
                    <button>Encode</button>
                    <button type="button" onClick={handleDecode}>Decode</button>
                </div>
            </div>
            <div className="form-row">
                <label>Encode message</label>
                <textarea value={payload.messageEncoded} onChange={handleChange} name="messageEncoded" id="messageEncoded" cols="30" rows="10"/>
            </div>
        </form>
    );
};

export default EncodeDecode;