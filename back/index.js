const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const cors = require("cors");
//===============================
const app = express();
app.use(express.json());
app.use(cors());
//===============================

const port = 8000;

app.get('/:word', (req, res) => {
    res.send(req.params.word);
});



app.post('/encode', (req, res) => {

    const result = Vigenere.Cipher(req.body.password).crypt(req.body.message);
    const response = {"encoded": result};
    res.send(response);
});

app.post('/decode', (req, res) => {
    const result = Vigenere.Decipher(req.body.password).crypt(req.body.message);
    const response = {"decoded": result};
    res.send(response);
});

app.listen(port, () => {
    console.log('we are live on ' + port);
});